//AJAX----------------------------------------------------------------------
//Devuelve un objeto listo para hacer peticiones http
function genAjx(){return window.XMLHttpRequest? new XMLHttpRequest(): new ActiveXObject("Microsoft.XMLHTTP");}

//metodo que ejecuta una peticion por metodo get e inserta la respuesta en un objeto
function gaxo(url,obj)
{
  var axobj=genAjx();
  axobj.onreadystatechange=function(){if (this.readyState==4 && this.status==200)obj.innerHTML=this.responseText};
  axobj.open("GET",url,true);
  axobj.send();
}

//metodo que ejecuta peticiones por metodo get y ejecuta una funcion
function gax(url,func)
{
  var axobj=genAjx();
  axobj.onreadystatechange=function(){if (this.readyState==4 && this.status==200)func(this.response)};
  axobj.open("GET",url,true);
  axobj.send();
}
//Metodo que envia una peticion por metodo post y luego ejecuta una funcion
function pax(url,params,func)
{
  var axobj=genAjx();
  axobj.onreadystatechange=function(){if (this.readyState==4 && this.status==200)func(this)};
  axobj.open("POST",url,true);
  axobj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  axobj.send(params);
}
//Metodo que envia una peticion por metodo post y luego imprime el resultado en nodo
function paxo(url,params,obj)
{
  var axobj=genAjx();
  axobj.onreadystatechange=function(){if (this.readyState==4 && this.status==200)obj.innerHTML=this.responseText};
  axobj.open("POST",url,true);
  axobj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  axobj.send(params);
}



//Funciones Genericas ------------------------------------------------------
function csstoggle(obj,css)
{
  obj.classList.toggle(css);
}
// Devuelve un nodo o una coleccion de nodos en base a una clase o un id
function $(str)
{
  return document.querySelector(str);
}


//botones del navegador------------------------------------------------------
// variable que contiene el arreglo de botones del navegador
var navbts;

//turnAllOff
//Este metodo sirve para apagar todos y cada uno de los botones del navegador
function taff()
{
  for (var i = 0; i < navbts.length; i++)
  if (navbts[i].classList.length>3)
  navbts[i].classList.toggle("selected");
}

//inicializa el comportamiento de los botnes de un navegador
//Parametros: id -> valor de la propiedad id de la etiqueta que contiene los botones
function  navbts_init(id)
{
  navbts=document.querySelector(id).children;
  for (var i = 0; i < navbts.length; i++)
    navbts[i].onclick=function()
    {
      taff();
      csstoggle(this,"selected");
    };
}
